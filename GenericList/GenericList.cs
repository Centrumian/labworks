﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace GenericList
{
    public class GenericList<T> : IList<T>
    {
        private GenericItem CItem;

        public GenericList()
        {
            CItem = new GenericItem(); // создаем отправную точку коллекции
        }

        // реализуем операцию индексирования
        public T this[int index]
        {
            get
            {
                var _cItem = CItem.Clone();
                while (_cItem.Value != null) // по ссылкам на предыдущие объекты перебираем их все
                { 
                    if (_cItem.Index == index) // когда нашли нужный
                        return (T)_cItem.Value;// возвращаем его
                    _cItem = _cItem.PrevItem;
                };
                throw new IndexOutOfRangeException(); // если такого индекса не обнаружено - выбрасываем исключение
            }
            set
            {
                // в акссесоре set делаем тоже самое
                var _cItem = CItem.Clone();
                while (_cItem.Value != null)
                {
                    if (_cItem.Index == index)
                        _cItem.Value = value; // только не возвращаем объект, а переопределяем его свойство 
                    _cItem = _cItem.PrevItem;
                };
                throw new IndexOutOfRangeException();
            }
        }

        public int Count => CItem.Index + 1; // возвращаем индекс последнего объекта + 1

        public bool IsReadOnly => false; // всегда доступна для редактирования

        public void Add(T item) =>  CItem = new GenericItem(item, CItem); // добавляем объект через параметризированный конструктор

        public void Clear() => CItem = new GenericItem(); // возвращаемся к отправной точки коллекции

        public bool Contains(T item)
        {
            var _cItem = CItem.Clone();
            while (_cItem.Value != null) // таким же образом перебираем все объекты
            {
                if (_cItem.Value.Equals(item)) // найдя нужный, возвраем true
                    return true;
                _cItem = _cItem.PrevItem;
            };
            return false; // иначе false
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            var _cItem = CItem.FirstItem.NextItem;
            while (_cItem.Value != null)
            {
                if (_cItem.Index == arrayIndex)
                    break;
                _cItem = _cItem.NextItem;
            }
            if (_cItem.Index != -1) // нашли первый объект, который надо перезаписать
            {
                foreach (T t in array) // в цикле по копируемому массиву
                {
                    try
                    {
                        _cItem.Value = t; // переопределяем значение каждого объекта
                        _cItem = _cItem.NextItem; // и переходим к следующему
                    }
                    catch
                    { 
                        throw new IndexOutOfRangeException(); // если дошли до конца списка, то выбрасываем исключение
                    }
                }
            }        
            else throw new IndexOutOfRangeException();
        }  

        public int IndexOf(T item)
        {
            var _cItem = CItem.Clone();
            while (_cItem.Value != null)
            {
                if (_cItem.Value.Equals(item))
                    return _cItem.Index; // если найден элемент с нужным индексом, то возвращаем его индекс
                if (_cItem.Index == -1)
                    return -1; // иначе -1
                _cItem = _cItem.PrevItem; 
            }
            return -1;
        }

        public void Insert(int index, T item)
        {
            var _cItem = CItem.Clone();
            while (_cItem.Value != null)
            {
                if (_cItem.Index == index - 1)
                    break;
                _cItem = _cItem.PrevItem;                  
            }
            if (_cItem.Index != -1) // нашли элемент с тем индексом, на который надо внедрить новый объект
            {
                // далее начинаем переопределять самый последний объект в нашей "цепочке" 
                CItem = new GenericItem(item, _cItem.Clone()); // добавили внедряемый объект
                CItem = new GenericItem(_cItem.NextItem.Value, _cItem.NextItem.Clone()); // добавили следом за ним  первый объект, из тех что у нас остались
                CItem.PrevItem.Value = item; // у предыдущего элемента необходимо переопределить его значение
                _cItem = _cItem.NextItem.NextItem; // идем дальше по ссылкам на предыдущие объекты...
                while (_cItem != null) 
                {
                    CItem = new GenericItem(_cItem.Value, CItem); // ... и добавляем их в конец цепочки
                    _cItem = _cItem.NextItem;
                }
            }
            else throw new IndexOutOfRangeException();
        }

        public bool Remove(T item)
        {
            var _cItem = CItem.Clone();
            while (_cItem.Value != null)
            {
                if (_cItem.Value.Equals(item))
                    break;
                _cItem = _cItem.PrevItem;
            }
            if (_cItem.Index != -1) // нашли удаляемый объект. Выкинем его из цепочки
            {
                CItem = new GenericItem(_cItem.NextItem.Value, _cItem.PrevItem); // оставим все объекты, что остались перед ним. И начнем дополнять их объектами, идущие следом за удаляемым
                if (_cItem.NextItem?.NextItem != null) // пока оставшиеся объекты не закончаться
                {
                    _cItem = _cItem.NextItem.NextItem;
                    while (_cItem != null)
                    {
                        CItem = new GenericItem(_cItem.Value, CItem); // тут добавляем к концу  оставшиеся объекты
                        _cItem = _cItem.NextItem;
                    }
                }
                return true;
            }
            return false;
        }

        public void RemoveAt(int index)
        {
            try
            {
                var _cItem = CItem.Clone();
                while (_cItem.Value != null)
                {
                    if (_cItem.Index == index)
                        break;
                    _cItem = _cItem.PrevItem;
                }
                // находим объект по удаляемому индексу
                Remove((T)_cItem.Value); // переходим к логике предыдущего метода
            }
            catch { } // в случае недопустимого индекса ничего не делаем
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new ItemEnumerator<T>(CItem); // вернем экземпляр нашего нумератора
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }       
        
        // Данный класс является основой всего листа. Каждый экземпляр хранит значение в поле типа object,
        // ссылку на предыдущий экземпляр и на следующий, а также индекс элемента в "мнимой" коллекции
        // По сути весь лист является оберткой вокруг одного экземляра данного класса. Все предыдущие объекты доступны через его поля
        // Переход к следующему объекту происходит через создание нового экземпляра по параметризированному конструктору.
        // Непараметризированный конструктор служит для созданий "начальной точки". Он не хранит никакое значение, и просто служит для обозначения
        // конца списка
        class GenericItem
        {
            public object Value { get; set; }

            public GenericItem PrevItem { get; set; }

            public GenericItem NextItem { get; set; }

            public GenericItem FirstItem { get; private set; }

            public int Index { get; set; }

            public GenericItem(object value, GenericItem prevItem)
            {
                prevItem.NextItem = this;
                Value = value;
                PrevItem = prevItem;
                Index = prevItem.Index + 1;
                FirstItem = prevItem.FirstItem;
            }

            public GenericItem()
            {
                Index = -1;
                FirstItem = this;
            }

            public GenericItem Clone()
            {
                GenericItem gi = new GenericItem();
                gi.FirstItem = this.FirstItem;
                gi.Index = this.Index;
                gi.NextItem = this.NextItem;
                gi.PrevItem = this.PrevItem;
                gi.Value = this.Value;
                return gi;
            }
        }

        class ItemEnumerator<T> : IEnumerator<T>
        {
            private GenericItem Item;

            public ItemEnumerator(GenericItem item)
            { Item = item.FirstItem.NextItem; } //  в конструкторе переходим к первому объекту в нашей цепочке

            public object Current => Item.Value; // возвращаем текущий объект

            T IEnumerator<T>.Current => (T)Item.Value;

            public void Dispose()
            {
               // диспозить нам ничего не надо
            }

            public bool MoveNext() // переходим к следующему объекту по хранимой ссылке
            {
                if (Item.NextItem == null)
                    return false;
                
                Item = Item.NextItem;
                return true;
            }

            public void Reset()
            {
                Item = Item.FirstItem; // в ресете возврашаемся к первому объекту 
            }
        }
    }
}
