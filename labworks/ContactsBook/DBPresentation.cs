﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using ContactsBook.Models;
using System.Reflection;

namespace ContactsBook
{
    /// <summary>
    /// Класс, представляющий собой имитацию базы данных. Рабочая коллекция сохраняется и считывается из файла по требованию
    /// Выбор в пользу такого решения обусловлен отстутствием проблем работы приложения на любой машине
    /// В отличие от использования какого нибудь IIS, который 99% пришлось бы настраивать, менять строку подключения и т.д.
    /// </summary>
        static class DBPresentation
        {
            private static string _userPath = "dbContacts.bin";
            private static BinaryFormatter _bf;

        // итак, при первом обращении к этому классу...
            static DBPresentation()
            {
                _bf = new BinaryFormatter();
                if (!File.Exists(_userPath)) // если нет файла с данными, то 
                {
                    using (FileStream fs = new FileStream(_userPath, FileMode.OpenOrCreate))
                    {
                    // создадим коллекцию контактов из одного пользователя
                        ObservableCollection<Contact> users = new ObservableCollection<Contact>();
                             users.Add(new Contact("Администратор", "89501112233", "89501112233", "admin@mail.ru", "admin", new DateTime(2018,1,1).ToShortDateString(), "Администрация сервиса"));

                    _bf.Serialize(fs, users); // и сохраним её
                    }
                }               
            }

        // получение текущей коллекции
            public static ObservableCollection<Contact> GetUsers()
            {
                using (FileStream fs = new FileStream(_userPath, FileMode.OpenOrCreate))
                {
                    return (ObservableCollection<Contact>)_bf.Deserialize(fs);
                }
            }

        // переопределение одного члена коллекции
        public static void Refresh(Contact oldcontact, Contact newcontact)
        {
            // считываем текущую коллекцию
            ObservableCollection<Contact> _current;
            using (FileStream fs = new FileStream(_userPath, FileMode.OpenOrCreate))
            {
                _current = (ObservableCollection<Contact>)_bf.Deserialize(fs);
            }
            int _c = _current.IndexOf(_current.Where(c => c.Equals(oldcontact)).First()); //находим индекс старого контакт
            _current.RemoveAt(_c); // удаляем его 
            _current.Insert(_c, newcontact.Clone()); // и записываем в коллекцию новый контакт
            using (FileStream fs = new FileStream(_userPath, FileMode.Create))
            {
                _bf.Serialize(fs, _current);
            }
        }

        public static void Add(Contact newcontact)
            {
                ObservableCollection<Contact> _current;
                using (FileStream fs = new FileStream(_userPath, FileMode.OpenOrCreate))
                {
                    _current = (ObservableCollection<Contact>)_bf.Deserialize(fs);
                }
                _current.Add(newcontact.Clone()); // просто добавляем в коллекцию новый контакт
                using (FileStream fs = new FileStream(_userPath, FileMode.Create))
                {
                    _bf.Serialize(fs, _current);
                }

            }

            public static void Delete(Contact record)
            {
                ObservableCollection<Contact> _current;
                using (FileStream fs = new FileStream(_userPath, FileMode.OpenOrCreate))
                {
                    _current = (ObservableCollection<Contact>)_bf.Deserialize(fs);
                }
                _current.Remove(record); // удаляем из коллекции контакт
                using (FileStream fs = new FileStream(_userPath, FileMode.Create))
                {
                    _bf.Serialize(fs, _current);
                }
            }
     }
}
