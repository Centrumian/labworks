﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ContactsBook.Models.Windows;

namespace ContactsBook
{

    class Commands
    {
        public static RoutedCommand Add;

        public static RoutedCommand AddClose;

        public static RoutedCommand Delete;

        public static RoutedCommand Refresh;

        public static RoutedCommand SelectToChar;

        static Commands()
        {
            Add = new RoutedCommand("Add", typeof(MainWindow));
            AddClose = new RoutedCommand("AddClose", typeof(AddingDialogWindow));
            Delete = new RoutedCommand("Delete", typeof(MainWindow));
            Refresh = new RoutedCommand("Refresh", typeof(MainWindow));
            SelectToChar = new RoutedCommand("SelectToChar", typeof(MainWindow));
        }
    }
}
