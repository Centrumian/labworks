﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ContactsBook.Models;
using ContactsBook.Models.Windows;

namespace ContactsBook
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Contact> CurrentContacts;
        Contact InfoContact;
        Contact OldInfoContact;
        bool isInfoContactChanged;

        public MainWindow()
        {
            InitializeComponent();
            CurrentContacts = DBPresentation.GetUsers(); // получаем коллекцию из нашей бд
            _xamlContacts.ItemsSource = CurrentContacts; // в источник листа контакт присваиваем эту коллекцию
            _xamlContacts.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("Name", System.ComponentModel.ListSortDirection.Ascending)); // задаем сразу же для этого листа порядок сортировки по имени
            ((INotifyCollectionChanged)_xamlContacts.ItemsSource).CollectionChanged += MainWindow_CollectionChanged; // подписываемся на изменение коллекции
            InfoContact = new Contact(); // текущий контакт всего окна. Его свойства выводятся в среднем блоке приложения
            _xamlInfo.DataContext = InfoContact; // создание привязки среднего блока и текущего контакта
        }

        private void MainWindow_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            _xamlContacts.Items.Refresh(); // обновляем коллекцию контактов в листе
            _xamlContacts.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("Name", System.ComponentModel.ListSortDirection.Ascending)); // и для обновленной коллекции придется еще раз задать порядок сортировки
        }

        // происходит при выборе контакта в листе контактов
        private void _xamlContacts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UnHighlightTextboxes(); // обновляем все текстбоксы
            InfoContact.PropertyChanged -= InfoContact_PropertyChanged; // отписываемся от события об изменении свойств, так как текущий контакт сейчас изменится, следить за старым нет необходимости
            InfoContact = _xamlContacts.SelectedItem as Contact; // и вот в текущий контакт записываем выбранный из листа. По привязке все текстбоксы в центральном блоке изменятся, так как изменился источник данных
            if (InfoContact != null) // если выделение в листе существует (бывает, что такого нет. Наример при запуске приложения текущий контакт не выбран или при удалении последнего контакт)
            {
                OldInfoContact = InfoContact.Clone();
                // создаем копию выбранного контакт. Это нужно когда хотим изменить свойства текущего
                // и так как мы работаем с ссылочными типами, то необходимо сделать именно копию (т.е. новый экземпляр класса с теми же свойствами)
                // чтобы при таком присвоении привязка UI элементов на потянулась за изменяемым контактом
                InfoContact.PropertyChanged += InfoContact_PropertyChanged; // подписывамся на событие об изменени свойств нового контакта
                _xamlInfo.DataContext = InfoContact; // и обновляем источник данных для текстбоксов
            }
            else
            {
                InfoContact = new Contact(); // если же выделения нет, то создадим новый экземпляр контакта с пустыми полями
                isInfoContactChanged = false; // флаг, сигнализирующий о том, что свойства экземпляра были изменены
            }
        }

        private void InfoContact_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // итак, какое то свойство изменилось
            isInfoContactChanged = true; // мы поднимаем об этом флаг
            HighlightTextbox(e.PropertyName); // и окрашиваем для видимости то поле, где произошло изменение
        }

        //выполняется при нажатии кнопки добавить
        private void CommandBindingAdd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var _addWin = new AddingDialogWindow(); // создаем диалоговое окно для добавления нового контакта  
            InfoContact = new Contact(); // обвновляем текущий контакт
            _addWin.DataContext = InfoContact; // для этого окна также присваиваем источник данных
            if (_addWin.ShowDialog() == true) // показываем это окно и ждем, что результатом его выполнения будет флаг true
            {
                // тогда
                isInfoContactChanged = false;
                DBPresentation.Add(InfoContact); // добавляем его в бд
                CurrentContacts.Add(InfoContact); // и в текущую коллекцию (а она является источником для листа контактов)
                UnHighlightTextboxes(); // снимаем все выделения с текстбоксов
                MainWindow_CollectionChanged(null, null); // и вызываем метод по обновлению коллекции главного окна
            }
        }

        // если же нажата кнопка удалить
        private void CommandBindingDelete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            isInfoContactChanged = false;
            DBPresentation.Delete(InfoContact); // удаляем объект из бд
            CurrentContacts.Remove(InfoContact); // из текущей коллекции
            InfoContact = new Contact(); // и обновляем текущий контакт
        }

        // если же нажата кнопка удалить
        private void CommandBindingRefresh_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            isInfoContactChanged = false; 
            DBPresentation.Refresh(OldInfoContact, InfoContact); // обновляем элемент в бд
            UnHighlightTextboxes(); 
            MainWindow_CollectionChanged(null, null);
        }

        //Далее идут три обработчика, определяющие может ли быть кнопка ( а точнее команда) доступна
        private void CommandBindingAdd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true; // кнопка нажать - всегда 
        }

        private void CommandBindingDelete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // кнопка удалить - если все поля текущего контакта заполнены правильно
            if (InfoContact != null)
                e.CanExecute = InfoContact.IsCorrect;
        }

        private void CommandBindingRefresh_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // кнопка обновить - если был поднят флаг о том, что свойство изменено и все поля контакта также заполнены правильно
            if (InfoContact != null)
                e.CanExecute = isInfoContactChanged && InfoContact.IsCorrect;
        }

        // это метод для подсветки какого либо текстбокса
        private void HighlightTextbox(string tag)
        {
            // из всех текстбоксов среднего блока окна находим те, у которых tag == измененному свойству
            var tb = _xamlInfo.Children.OfType<TextBox>().Where(t => t.Tag.ToString() == tag).FirstOrDefault();
            if (tb != null) // если такой найден, то меняем ему заливку на светло-желтый
            {
                tb.Background = Brushes.LightYellow;
            }
        }

        // это метод для снятия всех заливок
        private void UnHighlightTextboxes()
        {
            // у всех текстбоксов среднего блока возвращает заливку в исходное состояние
            foreach (var tb in _xamlInfo.Children.OfType<TextBox>())
            {
                tb.Background = Brushes.White;
            }
           
        }

        // это событие загрузки окна приложения
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // этот блок кода для генерации кнопок с буквами, по нажатию на которые будут выполняться команды
            char[] az = Enumerable.Range('а', 'я' - 'а' + 1).Select(i => (Char)i).ToArray(); // создаем массив букв русского алфавита
            for (int i=0; i< az.Count(); i ++) // в цикле по этому массиву
            {
                Button b = new Button(); // создаем кнопку
                b.Background = Brushes.White; // устанавливаем ей заливку
                b.Width = 20; // и размеры
                b.Height = 20;
                b.Content = az[i].ToString(); // и отображаемый текст
                b.Command = Commands.SelectToChar; // а также присваиваем команду для выполнения действия
                _xamlChars.Children.Add(b); // и добавляем эту кнопку в панель, рядом со списком контактов
            }

            // этот блок кода для генерации списка контактов, у которых скоро др
            int _maxDates = 5; // зададимся максимум
            List<Contact> _nearestsBdates = new List<Contact>();

            while (_nearestsBdates.Count < _maxDates) // цикл будет выполняться, пока не наберем достаточное количество контактов из списка
            {
                // для того, чтобы найти самые ближайшие др, надо все даты рождений привести к одному году
                // пусть это будет год 0001, и тогда мы можем работать с датами, как с простыми числами
                DateTime _dtNow = new DateTime(1, DateTime.Now.Month, DateTime.Now.Day); // берем сегодняшний день
                List<Tuple<DateTime, Contact>> _dts = new List<Tuple<DateTime, Contact>>();
                foreach (var c in CurrentContacts) // проходимся по всем контактам и из каждого создадим tuple
                {
                    DateTime _t = DateTime.Parse(c.BirthDate); // берем др контакта
                    DateTime _dtCon = new DateTime(1, _t.Month, _t.Day); // также приводим его к 0001 году 
                    _dts.Add(new Tuple<DateTime, Contact>(_dtCon, c)); // и сохраняем в коллекции tuples
                }
                // на следующем шаге будем искать самый ближайший др к сегодняшней дате. для этого
                // 1. Отфильтруем все контакты, которые уже есть в нашем списке (дублировать же их не надо)
                // 2. Отсортируем в порядке возрастания разницы дат между сегодняшним днем и др
                // 3. Берем первый из отсортированных элементов - это и будет ближайший!
                Contact con = _dts.Where(t => !_nearestsBdates.Contains(t.Item2)).OrderBy(tt => _dtNow - tt.Item1).Select(ttt=> ttt.Item2).FirstOrDefault();
                if (con == null) // если же книга контаков меньше, чем максимум то просто выходим из цикла
                    break;
                _nearestsBdates.Add(con);
            }
            _xamlNearestBirths.ItemsSource = _nearestsBdates.Select(nb => nb.BirthDate + " -- " + nb.Name); // для листа с ближайшими датами задаем источник в виде полученного нами списка
            
        }

        // это команда для кнопки с буквой алфавита
        private void CommandBindingSelectToChar_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Button btn = e.OriginalSource as Button; // получаем кнопку, которая была нажата
            // среди всех контактов ищем первый такой, у которого первая буква равна букве на кнопке
            Contact con = CurrentContacts.FirstOrDefault(c => c.Name[0].ToString().ToLower() == btn.Content.ToString().ToLower());
            if (con != null) // если такой найден
            {
                _xamlContacts.SelectedItem = con; // то выделяем этот контакт в списке
            }
        }

        // этот обработчик событий - изменения текста в строке поиска
        // на самом деле на месте списка всех контактов есть еще один список - отсортированных контактов по вхождения строки в одно из полей контакта
        // но такой список контактов скрыт до того момента, пока не будет найден какой либо контакт, удволетворяющий введеной строке
        private void _xamlAlphPointer_TextChanged(object sender, TextChangedEventArgs e)
        {
            _xamlAsyncFilter.ItemsSource = null; // каждый раз очищаем этот список
            if (!string.IsNullOrEmpty(_xamlAlphPointer.Text))
            {
                // итак, если введенная строка не пуста, то..
                string txt = _xamlAlphPointer.Text;
                List<Contact> _filteredContcs = new List<Contact>();
                // применяем библиотеку reactive extensions
                // вызываем следующий метод асинхронно...
                var o = Observable.Start(() =>
                {
                    // метод : в списке контактов ищем такие, у которых хоть одно свойство содержит введенную строку
                    // причем перед сравнением приводим все строки к нижнему регистру
                    _filteredContcs = CurrentContacts.Where(c => c.Name.ToLower().Contains(txt.ToLower())
                                                              || c.HomeNumber.ToLower().Contains(txt.ToLower())
                                                              || c.WorkNumber.ToLower().Contains(txt.ToLower())
                                                              || c.Skype.ToLower().Contains(txt.ToLower())
                                                              || c.Email.ToLower().Contains(txt.ToLower())
                                                              || c.BirthDate.ToLower().Contains(txt.ToLower())
                                                              || (!string.IsNullOrEmpty(c.Comment) && c.Comment.ToLower().Contains(txt.ToLower()))
                                                              ).ToList();

                });
                o.Wait(); //  дожидаемся результата поиска
                if (_filteredContcs.Any()) // если найдены хоть какие нибудь контакты, то скрываем общий список контактов, и показываем отфильтрованный
                {
                    _xamlAsyncFilter.Visibility = Visibility.Visible;
                    _xamlContacts.Visibility = Visibility.Hidden;
                    _xamlAsyncFilter.ItemsSource = _filteredContcs; // источником отфильтрованных контактов являются найденные нами контакты
                    // точно также как с обычным списком контактов
                }
                else
                {
                    // если же не найдены, то все возвращем в исходное состояние
                    _xamlContacts.Visibility = Visibility.Visible;
                    _xamlAsyncFilter.Visibility = Visibility.Hidden;
                }
            }
            else
            {
                _xamlContacts.Visibility = Visibility.Visible;
                _xamlAsyncFilter.Visibility = Visibility.Hidden;
            }
        }

        // это обработчик выбора в отфильтрованном списке
        private void _xamlAsyncFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {    
            // так как после выбора контакта в отфильтрованном списке, он нам не нужен, то все вернем в исходное состояние
            _xamlContacts.Visibility = Visibility.Visible;
            _xamlAsyncFilter.Visibility = Visibility.Hidden;
            if (_xamlAsyncFilter.SelectedItem != null) // итак, если был выбран контакт в отфильтрованном контакте
            {
                // то в общем списке контактов находим по имени выбранный в отфильтрованном
                Contact _selCont = CurrentContacts.First(c => c.Name == _xamlAsyncFilter.SelectedItem.ToString());
                _xamlContacts.SelectedItem = _selCont; // и выбираем его в списке контактов
                _xamlAsyncFilter.ItemsSource = null;
            }
        }
    }
}
