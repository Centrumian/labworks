﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ContactsBook.Models
{
    [Serializable]
    class Contact : INotifyPropertyChanged
    {
        string _name;
        string _homeNumber;
        string _workNumber;
        string _email;
        string _skype;
        string _birthDate;
        string _comment;

        public string Name
        {
            get { return _name; }
            set
            {
                OnPropertyChanged("Name");
                _name = value;
            }
        }

        public string HomeNumber { get { return _homeNumber; } set { _homeNumber = value; OnPropertyChanged("HomeNumber"); } }

        public string WorkNumber { get { return _workNumber; } set { _workNumber = value; OnPropertyChanged("WorkNumber"); } }

        public string Email { get { return _email; } set { _email = value; OnPropertyChanged("Email"); } }

        public string Skype { get { return _skype; } set { _skype = value; OnPropertyChanged("Skype"); } }

        public string BirthDate { get { return _birthDate; } set { _birthDate = value; OnPropertyChanged("BirthDate"); } }

        public string Comment { get { return _comment; } set { _comment = value; OnPropertyChanged("Comment"); } }

        public Contact() { }

        public Contact(string name, string homeNumber, string workNumber, string email, string skype, string birthDate, string comment)
        {
            _name = name;
            _homeNumber = homeNumber;
            _workNumber = workNumber;
            _email = email;
            _skype = skype;
            _birthDate = birthDate;
            _comment = comment;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public override string ToString()
        {
            return _name;
        }

        public override bool Equals(object obj)
        {
            Contact _c = obj as Contact;
            if (_c == null)
                return false;
            else if (this.Name == _c.Name
                    && this.HomeNumber == _c.HomeNumber
                    && this.WorkNumber == _c.WorkNumber
                    && this.Email == _c.Email
                    && this.Skype == _c.Skype
                    && this.BirthDate == _c.BirthDate
                    && this.Comment == _c.Comment)
                return true;
            else return false;
        }

        public Contact Clone()
        {
            return new Contact(Name, HomeNumber, WorkNumber, Email, Skype, BirthDate, Comment);
        }

        public bool IsCorrect
        {
            get
            {
                return Name != null &&
                       HomeNumber != null && Regex.Match(HomeNumber, @"^(\+[0-9]{11})$").Success && 
                       WorkNumber != null && Regex.Match(WorkNumber, @"^(\+[0-9]{11})$").Success &&
                       Skype != null &&
                       Email != null &&
                       BirthDate != null &&
                       DateTime.TryParse(BirthDate, out DateTime dt);
            }
        }
    }
}
