﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ContactsBook.Models;

namespace ContactsBook.Models.Windows
{
    /// <summary>
    /// Interaction logic for AddingDialogWindow.xaml
    /// </summary>
    public partial class AddingDialogWindow : Window
    {
        public AddingDialogWindow()
        {
            InitializeComponent();
        }

        // проверяем, доступна ли команда ( а значит и кнопка добавить для выпонления)
        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            Contact _dataContact = this.DataContext as Contact;
            if (_dataContact != null)
            {
                e.CanExecute = _dataContact.IsCorrect;  // если все текстбоксы заполнены правильно                      
            }
            else
                e.CanExecute = false;
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true; // и вот если кнопка доступна и мы ее нажимаем, то это диалоговое окно будет закрыто
        }
    }
}
