﻿using GenericList;
using System;
using System.Windows;

namespace TestGenericListConsole
{
    class Program
    {
        static GenericList<double> _doubles;
        static void Main(string[] args)
        {
            GenericList<Person> wins = new GenericList<Person>();
            _doubles = new GenericList<double>();
            _doubles.Add(1);
            _doubles.Add(2);
            _doubles.Add(10);

            WriteCollection();

            Console.WriteLine("Элемент под индексом 1");
            Console.WriteLine(_doubles[1]);

            Console.WriteLine("Количество элементов");
            Console.WriteLine(_doubles.Count);

            Console.WriteLine("Добавим объект 5"); _doubles.Add(5);
            WriteCollection();

            Console.WriteLine("Содержит ли коллекция обект 10");
            Console.WriteLine(_doubles.Contains(10));
            Console.WriteLine("Содержит ли коллекция обект 13");
            Console.WriteLine(_doubles.Contains(13));

            Console.WriteLine("Индекс объекта 2");
            Console.WriteLine(_doubles.IndexOf(2));

            Console.WriteLine("Вставим элемент 11 в индекс 1");
            _doubles.Insert(1, 11);
            WriteCollection();

            Console.WriteLine("Удалим элемент 11");
            _doubles.Remove(11);
            WriteCollection();


            Console.ReadLine();
        }

        static private void WriteCollection()
        {
            Console.WriteLine("Текущая коллекция");
            foreach (var d in _doubles)
            {
                Console.Write(d + "\t");
            }
            Console.WriteLine();
        }
    }

    class Person
    {

    }
}
