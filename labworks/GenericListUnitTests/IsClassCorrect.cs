﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GenericList;

namespace GenericListUnitTests
{
    /// <summary>
    /// Класс теста. 
    /// Каждый тестовый метод является проверкой для соответствующего метода generic коллекции
    /// В каждом из них происходят элементарные операции
    /// Положительным исходом теста является соответствие ожидаемого и полученного результата
    /// </summary>
    [TestClass]
    public class IsClassCorrect
    {

        [TestMethod]
        public void Valid_Count()
        {
            GenericList<object> _objs = new GenericList<object>();
            _objs.Add(new object());
            Assert.AreEqual(1, _objs.Count);
        }

        [TestMethod]
        public void Valid_Add()
        {
            GenericList<object> _objs = new GenericList<object>();
            _objs.Add(new object());
            Assert.AreEqual(true, _objs[0] != null);
        }

        [TestMethod]
        public void Valid_Clear()
        {
            GenericList<object> _objs = new GenericList<object>();
            _objs.Add(new object());
            _objs.Clear();
            Assert.AreEqual(0, _objs.Count);
        }

        [TestMethod]
        public void Valid_Contains()
        {
            GenericList<double> _objs = new GenericList<double>();
            _objs.Add(1);
            _objs.Add(2);
            _objs.Add(3);
            Assert.AreEqual(true, _objs.Contains(2));
            Assert.AreEqual(false, _objs.Contains(5));
        }

        [TestMethod]
        public void Valid_CopyTo()
        {
            GenericList<double> _objs = new GenericList<double>();
            _objs.Add(1);
            _objs.Add(2);
            _objs.Add(3);
            int _insertIndex = 1;
            double[] _d2 = new double[2] { 4, 5 };
            _objs.CopyTo(_d2, _insertIndex);
            Assert.AreEqual(_d2[0], _objs[_insertIndex]);
            Assert.AreEqual(_d2[1], _objs[_insertIndex + 1]);
        }

        [TestMethod]
        public void Valid_GetEnumerator()
        {
            GenericList<double> _dec = new GenericList<double>();
            double[] _d2 = new double[2] { 4, 5 };
            foreach (var d in _d2) _dec.Add(d);
            Assert.AreEqual(_d2.GetEnumerator().MoveNext(), _dec.GetEnumerator().MoveNext());
        }

        [TestMethod]
        public void Valid_IndexOf()
        {
            GenericList<double> _dec = new GenericList<double>();
            double _v = 2;
            _dec.Add(1);
            _dec.Add(_v);
            _dec.Add(3);
            Assert.AreEqual(1, _dec.IndexOf(_v));
        }

        [TestMethod]
        public void Valid_Insert()
        {
            GenericList<double> _dec = new GenericList<double>();
            _dec.Add(1);
            _dec.Add(2);
            _dec.Add(3);
            _dec.Insert(1, 5);

            Assert.AreEqual(4, _dec.Count);
            Assert.AreEqual(5, _dec[1]);
        }

        [TestMethod]
        public void Valid_Remove()
        {
            GenericList<double> _dec = new GenericList<double>();
            _dec.Add(1);
            _dec.Add(2);
            _dec.Add(3);
            _dec.Remove(1);

            Assert.AreEqual(2, _dec[0]);
            Assert.AreEqual(2, _dec.Count);
        }

        [TestMethod]
        public void Valid_RemoveAt()
        {
            GenericList<double> _dec = new GenericList<double>();
            _dec.Add(1);
            _dec.Add(2);
            _dec.Add(3);
            _dec.RemoveAt(0);

            Assert.AreEqual(2, _dec[0]);
            Assert.AreEqual(2, _dec.Count);
        }

    }
}
