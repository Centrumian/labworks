﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Calculator
{
    /// <summary>
    /// Через команду Put в текстовое поле калькулятора вводится какой то символ
    /// Данная команда применяется через стиль ко всем кнопкам на окне
    /// Реализацию ее логики см. в MainWindow.xaml.cs
    /// </summary>
    class Commands
    {
        public static RoutedCommand Put { get; set; }

        static Commands()
        {
            Put = new RoutedCommand("Put", typeof(MainWindow));
        }
    }
}
