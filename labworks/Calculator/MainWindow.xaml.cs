﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using org.mariuszgromada.math;
using math = org.mariuszgromada.math.mxparser;

namespace Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            // для красоты сделаем возможным заполнять поле вывода калькулятора через нажатия на клавиатура
            // для этого подпишемся на событие о нажатии клавиши
            this.KeyDown += MainWindow_KeyDown;
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            // в данном блоке определяется клавиша с каклй операцией была нажата и тем самым вызываем команду Put  с определенным параметром
            if (e.Key == Key.Divide) Commands.Put.Execute("Div", null);
            if (e.Key == Key.Multiply) Commands.Put.Execute("Mult", null);
            if (e.Key == Key.OemPlus || e.Key == Key.Add) Commands.Put.Execute("Sum", null);
            if (e.Key == Key.OemMinus || e.Key == Key.Subtract) Commands.Put.Execute("Minus", null);
            if (e.Key == Key.Enter) Commands.Put.Execute("Equals", null);
            if (e.Key == Key.Decimal) Commands.Put.Execute("Point", null);
            // в случае клавиши с цифровой необходима небольшая доработка
            char[] _chars = e.Key.ToString().ToCharArray(); // получаем имя клавиши (которе не просто цифра)
            char s = _chars.Where(c => char.IsDigit(c)).FirstOrDefault(); // переводим его в массив чаров, и оставляем только те символы, которые являются цифрами
            if (s != 0) // если такие символы есть, то запускаем команду с соответствующим параметром
                Commands.Put.Execute(s, null);
        }

        /// <summary>
        /// Вот реализация логики команды Put
        /// </summary>
        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string res = "";
            // Возможны два случая вызова этой команды:
            // 1. Через нажатие на клавишу в окне: тогда источник команды будет определен, а параметр равен null
            // 2. Вызываем команды из кода (при нажатии клавиши на клавиатуре) и присваем ей определенный параметр
            if (e.Parameter == null)
                res = (e.Source as Button).Name.Replace("_xaml", ""); // тогда в строку записываем, какая кнопка была нажата
            else
            {
                res = e.Parameter.ToString(); // считываем этот параметр в строку
            }
                
            string _resToShow = ""; // это именна та строка (а точнее символ), которая будет выведена в строку калькулятора
            if (res == "CE") // если нажата клавиша CE, то полностью очищаем поле вывода
            {
                _xamlInputTb.Text = "";
            }
            else if (res == "C") // если нажата клавиша C, то удаляем последний символ в поле вывода
            {
                int i = _xamlInputTb.Text.Length - 1;
                if (i >= 0)
                    _xamlInputTb.Text = _xamlInputTb.Text.Remove(i, 1);
            }
            else // теперь обработка нажатия клавиш с цифрой или какой ли арифметической операцией
            {
                switch (res)
                {
                    case "Sum": // если нажата клавиша +
                        {
                            _resToShow = "+";
                        }
                        break;
                    case "Minus": // если нажата клавиша -
                        {
                            _resToShow = "-";
                        }
                        break;
                    case "Mult": // если нажата клавиша *
                        {
                            _resToShow = "*";
                        }
                        break;
                    case "Div": // если нажата клавиша /
                        {
                            _resToShow = "/";
                        }
                        break;
                    case "Point": // если нажата клавиша .
                        {
                            _resToShow = ",";
                        }
                        break;
                    case "Equals": // если нажата клавиша =. Происходит обработка и вычисление результата
                        {              
                            math.Expression ex = new math.Expression(_xamlInputTb.Text.Replace(',','.')); // из строки вывода берем текст, меняем все запятые на точки (чтобы при русской раскладке не возникало ошибок)
                            string d = Math.Round(ex.calculate(),2).ToString(); // парсим строку в выражение и вычисляем его, округляя до двух знаков после запятой
                            string _tempProtocol = _xamlProtocol.Text as string; // берем текст в протоколе калькулятора и сохраняем во временную переменную
                            _xamlProtocol.Text = _xamlInputTb.Text + "=" + d + "\n"; // переопределяем содержание протокола
                            _xamlProtocol.Text += _tempProtocol; // а затем добавляем сохраненную раннее строку. Это позволяет имитировать добавление новой строки в самый верх блока
                            _xamlInputTb.Text = d; // и полученный результат записываем в поле вывода
                        }
                        break;
                    default: // если нажата какая либо цифра
                        {
                            _resToShow = res;
                        }
                        break;
                }
            }
            _xamlInputTb.Text += _resToShow; // после всех обработок добавляем символ к выражению в строке вывода
        }
    }
}
